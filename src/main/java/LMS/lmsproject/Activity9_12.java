package LMS.lmsproject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity9_12 {

	
	WebDriver driver;
	WebDriverWait wait;
	
    @BeforeTest
    public void setUp() {
        
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\ZankhnaDarji\\Downloads\\chromedriver_win32\\chromedriver.exe");
    	driver=new ChromeDriver();	        
        driver.get("https://alchemy.hguy.co/lms");   
        driver.manage().window().maximize();
    }
    
    
    @Test
    public void Activity9toDo() throws InterruptedException{
    	
    	wait= new WebDriverWait(driver,20);
    	JavascriptExecutor je = (JavascriptExecutor) driver;
    	WebElement menuUL= driver.findElement(By.id("primary-menu"));
    	List<WebElement> menulinks=menuUL.findElements(By.tagName("li"));
    	for (WebElement li : menulinks) {
    	if (li.getText().equals("My Account")) {
    	     li.click();
    	     break;
    	   }
    	}
    	
    	WebElement button=driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div[2]/div[2]/div[2]/a"));
   		button.click();
    	driver.findElement(By.id("user_login")).sendKeys("abc");
    	driver.findElement(By.id("user_pass")).sendKeys("Passw0rd");
    	driver.findElement(By.xpath("//*[@id=\"wp-submit\"]")).click(); 
    	
    	
    	driver.findElement(By.xpath("/html/body/div[1]/header/div/div/div/div/div[3]/div/nav/div/ul/li[2]/a")).click();
    	
    	//course
    	WebElement courselink=driver.findElement(By.xpath("//*[@id=\"post-69\"]/a"));
    	Thread.sleep(2000);
    	je.executeScript("arguments[0].scrollIntoView(true);",courselink);
     	courselink.click();
    	
    	    	   	 
    	WebElement element = driver.findElement(By.xpath("//*[@id=\"learndash_post_69\"]/div/div[3]/div[1]/h2"));
    			
    	Thread.sleep(2000);	 
    	je.executeScript("arguments[0].scrollIntoView(true);",element);
    	  	
    	
    	
    	driver.findElement(By.xpath("//a[contains(@class,'ld-item-name ld-primary-color-hover')]")).click();
    	
    	
    	System.out.println(driver.getTitle());
    	
    	Assert.assertEquals(driver.getTitle(), "Developing Strategy – Alchemy LMS");
    	
    	
    	
    	//Thread.sleep(2000);
    	
    	try 
    	{
    		WebElement markButton=driver.findElement(By.className("learndash_mark_complete_button"));
    		if(markButton.isDisplayed())
			{
    			markButton.click();
			}
    	
    	}
    	
    	catch(Exception e)
    	{
    		System.out.println("exception:"+e.getMessage());
    	}
    	/*Thread.sleep(2000);
    	driver.findElement(By.xpath("//a[contains(@href,'monitoring')]")).click();
    	Thread.sleep(2000);
    	driver.findElement(By.xpath("//a[contains(@href,'influencing')]")).click();
    	
    	System.out.println("first lesson completed..");
    	Thread.sleep(2000);
    	driver.findElement(By.xpath("//a[contains(@href,'developing-strategy')]")).click(); */
    	
    	 
    }
    
    @AfterTest
    public void tearDown() {        
        driver.close();
    }
    
}
