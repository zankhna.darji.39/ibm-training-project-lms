package LMS.lmsproject;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity14 {

	
	WebDriver driver;
	WebDriverWait wait;
	
    @BeforeTest
    public void setUp() {
        
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\ZankhnaDarji\\Downloads\\chromedriver_win32\\chromedriver.exe");
    	driver=new ChromeDriver();	        
        driver.get("https://alchemy.hguy.co/lms");   
        driver.manage().window().maximize();
    }
    
    
    @Test
    public void Activity14toDo() throws InterruptedException{
    	Actions actions=new Actions(driver);
    	wait= new WebDriverWait(driver,20);
    	JavascriptExecutor je = (JavascriptExecutor) driver;
    	WebElement menuUL= driver.findElement(By.id("primary-menu"));
    	List<WebElement> menulinks=menuUL.findElements(By.tagName("li"));
    	for (WebElement li : menulinks) {
    	if (li.getText().equals("My Account")) {
    	     li.click();
    	     break;
    	   }
    	}
    	
    	
    	
    	WebElement button=driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div[2]/div[2]/div[2]/a"));
   		button.click();
   		
    	
    	driver.findElement(By.id("user_login")).sendKeys("abc");
    	driver.findElement(By.id("user_pass")).sendKeys("Passw0rd");
    	Action actionSequence1 = actions.sendKeys("\uE007").build();
    	actionSequence1.perform();
    	 
    	
    	
    	WebElement courses=driver.findElement(By.xpath("/html/body/div[1]/header/div/div/div/div/div[3]/div/nav/div/ul/li[2]/a"));
    	actions.moveToElement(courses).click().perform();
    	
    	
    	
    	WebElement courselink=driver.findElement(By.xpath("//*[@id=\"post-71\"]/a"));
    	Thread.sleep(2000);
    	je.executeScript("arguments[0].scrollIntoView(true);",courselink);
     	actions.moveToElement(courselink).click().perform();
    	
	   	 
     	WebElement element = driver.findElement(By.xpath("//*[@id=\"learndash_post_71\"]/div/div[3]/div[1]/h2"));
			
     	Thread.sleep(2000);	 
     	je.executeScript("arguments[0].scrollIntoView(true);",element);
	  	
	
     	WebElement lesson=driver.findElement(By.xpath("//a[contains(@class,'ld-item-name ld-primary-color-hover')]"));
     	actions.moveToElement(lesson).click().perform();
		
     	System.out.println(driver.getTitle());
     	
     	Thread.sleep(2000);
     	WebElement bu1=driver.findElement(By.className("learndash_mark_complete_button"));
     	actions.moveToElement(bu1).click().perform();
		
     	Thread.sleep(2000);
     	//second topic
     	bu1=driver.findElement(By.className("learndash_mark_complete_button"));
     	actions.moveToElement(bu1).click().perform();
     	
     	Thread.sleep(2000);
     	//third topic
     	bu1=driver.findElement(By.className("learndash_mark_complete_button"));
     	actions.moveToElement(bu1).click().perform();
     	
     	
     	String progress=driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/div/div/div/div[1]/div[1]/div[1]/div/div[1]")).getText();
     	System.out.println("Course progress:"+progress);
    }
    
    @AfterTest
    public void tearDown() {        
        driver.close();
    }
    
}
