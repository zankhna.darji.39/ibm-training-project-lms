package LMS.lmsproject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity5 {

	
	WebDriver driver;
	WebDriverWait wait;
	
    @BeforeTest
    public void setUp() {
        
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\ZankhnaDarji\\Downloads\\chromedriver_win32\\chromedriver.exe");
    	driver=new ChromeDriver();	        
        driver.get("https://alchemy.hguy.co/lms");   
        driver.manage().window().maximize();
    }
    
    

    @Test
    public void Activity5toDo(){
    	
    	wait= new WebDriverWait(driver,20);
    	WebElement menuUL= driver.findElement(By.id("primary-menu"));
    	List<WebElement> menulinks=menuUL.findElements(By.tagName("li"));
    	for (WebElement li : menulinks) {
    	if (li.getText().equals("My Account")) {
    	     li.click();
    	   }
    	}
    	
    	String title=driver.getTitle();
    	
    	System.out.println(title);
    	if(title.equals("My Account – Alchemy LMS"))
    	{
    		driver.close();
    	}
    	
    }
    
    @AfterTest
    public void tearDown() {        
        //driver.close();
    }
    
}
