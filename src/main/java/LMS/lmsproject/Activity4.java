package LMS.lmsproject;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity4 {

	
	WebDriver driver;
		
    @BeforeTest
    public void setUp() {
        
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\ZankhnaDarji\\Downloads\\chromedriver_win32\\chromedriver.exe");
    	driver=new ChromeDriver();	        
        driver.get("https://alchemy.hguy.co/lms");       
    }
    
    

    @Test
    public void Activity4toDo(){
    	
    	List<WebElement> courses=driver.findElements(By.className("entry-title"));
    	
    	/*Iterator<WebElement> iterator = courses.iterator();
		
		while(iterator.hasNext()){
			
			System.out.println(iterator.next().getText());
		}*/
    	
		System.out.println("Second most popular course: "+courses.get(1).getText());
		if(courses.get(1).equals("Email Marketing Strategies"))
		{
			driver.close();
		}
    	    	
    }
    
    @AfterTest
    public void tearDown() {        
//        driver.close();
    }
    
}
