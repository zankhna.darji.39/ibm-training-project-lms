package LMS.lmsproject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity8 {

	
	WebDriver driver;
	WebDriverWait wait;
	
    @BeforeTest
    public void setUp() {
        
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\ZankhnaDarji\\Downloads\\chromedriver_win32\\chromedriver.exe");
    	driver=new ChromeDriver();	        
    	driver.manage().window().maximize();
    	driver.get("https://alchemy.hguy.co/lms");   
        
    }
    
    

    @Test
    public void Activity8toDo() throws InterruptedException{
    	
    	//wait= new WebDriverWait(driver,20);
    	WebElement menuUL= driver.findElement(By.id("primary-menu"));
    	List<WebElement> menulinks=menuUL.findElements(By.tagName("li"));
    	for (WebElement li : menulinks) {
    	if (li.getText().equals("Contact")) {
    	     li.click();
    	     System.out.println("navigated to contact page..");
    	     break;
    	   }
    	}
    	
    	
    	JavascriptExecutor je = (JavascriptExecutor) driver;    	   	 
    	WebElement element = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[3]/div[2]/div/h2"));
    	   	 
    	je.executeScript("arguments[0].scrollIntoView(true);",element);
    	 
    	
    	
    	
    	driver.findElement(By.id("wpforms-8-field_0")).sendKeys("Zankhna Darji");
    	driver.findElement(By.id("wpforms-8-field_1")).sendKeys("zan@abc.com");
    	driver.findElement(By.id("wpforms-8-field_3")).sendKeys("course inquiry");
    	driver.findElement(By.id("wpforms-8-field_2")).sendKeys("regarding the marketing course");
    	
    	
    	driver.findElement(By.name("wpforms[submit]")).click(); 
    	System.out.println(driver.findElement(By.xpath("//*[@id=\"wpforms-confirmation-8\"]/p")).getText());
    	
    }
    
    @AfterTest
    public void tearDown() {        
        driver.close();
    }
    
}
