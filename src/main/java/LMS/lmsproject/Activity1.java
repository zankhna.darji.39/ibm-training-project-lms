package LMS.lmsproject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity1 {

	
	WebDriver driver;
	String title;
	
    @BeforeTest
    public void setUp() {
        
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\ZankhnaDarji\\Downloads\\chromedriver_win32\\chromedriver.exe");
    	driver=new ChromeDriver();	        
        driver.get("https://alchemy.hguy.co/lms");       
    }
    
    

    @Test
    public void Activity1toDo(){
    	
    	title=driver.getTitle();
    	System.out.println(title);
    	if(title.equals("Alchemy LMS – An LMS Application"))
    	{
    		driver.close();
    	}
    	
    }
    
    @AfterTest
    public void tearDown() {        
        //driver.close();
    }
    
}
