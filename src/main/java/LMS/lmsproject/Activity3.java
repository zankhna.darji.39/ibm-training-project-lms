package LMS.lmsproject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity3 {

	
	WebDriver driver;
	WebElement header;
	
    @BeforeTest
    public void setUp() {
        
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\ZankhnaDarji\\Downloads\\chromedriver_win32\\chromedriver.exe");
    	driver=new ChromeDriver();	        
        driver.get("https://alchemy.hguy.co/lms");       
    }
    
    

    @Test
    public void Activity3toDo(){
    	
    	header=driver.findElement(By.tagName("h3"));
    	System.out.println(header.getText());
    	if(header.getText().equals("Actionable Training"))
    	{
    		driver.close();
    	}
    	
    }
    
    @AfterTest
    public void tearDown() {        
        //driver.close();
    }
    
}
